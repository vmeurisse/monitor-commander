#! /usr/bin/env python3
# PYTHON_ARGCOMPLETE_OK

import scripts.with_poetry as _  # isort:skip

import os
from argparse import ArgumentParser
from pathlib import Path
from subprocess import run
from typing import List, Sequence, Union

from argcomplete import autocomplete


def _set_in_path(path: Union[str, Path]):
    path_before = os.environ["PATH"]
    to_add = str(path)
    if not to_add in path_before.split(os.pathsep):
        os.environ["PATH"] = f"{to_add}{os.pathsep}{path_before}"


def _set_path_in_env():
    root = Path(__file__).parent
    _set_in_path(root / "node_modules/.bin")


def _list_python_files(sources: Sequence[Path]):
    files: List[Path] = []
    for path in sources:
        if path.is_file():
            files.append(path)
        else:
            files.extend(path.glob("**/*.py"))
    return files


def _lint_format(lint: bool):
    targets = [Path("monitor_commander"), Path("scripts"), Path("make.py")]
    isort_args = ["--check-only"] if lint else []
    black_args = ["--check"] if lint else []
    autoflake_args = [
        "--in-place",
        "-r",
        "--remove-all-unused-imports",
        "--remove-unused-variables",
        "--ignore-init-module-imports",
    ]

    if not lint:
        print("* autoflake")
        # Auto remove unused variables
        run(["autoflake", *autoflake_args, *targets], check=True)

    print("* isort")
    # Auto sort imports
    run(["isort", *isort_args, *targets], check=True)

    print("* black")
    # Auto format
    run(["black", *black_args, *targets], check=True)

    print("* pyright")
    # Check Types
    run(["pyright", *targets], check=True)

    print("* pylint")
    # Code corectness
    run(["pylint", *_list_python_files(targets)], check=True)

    print("* bandit")
    # Code security
    run(["bandit", "-c", "pyproject.toml", "-r", *targets], check=True)


def _format():
    _lint_format(False)


def _ci():
    _lint_format(True)


def _main():
    parser = ArgumentParser()

    subparsers = parser.add_subparsers(metavar="action", required=True)

    subparser = subparsers.add_parser("format")
    subparser.set_defaults(action=_format)

    subparser = subparsers.add_parser("ci")
    subparser.set_defaults(action=_ci)

    autocomplete(parser)
    args = parser.parse_args()

    _set_path_in_env()
    args.action()


if __name__ == "__main__":
    _main()
